<?php

namespace ptmc\settings;


use ptmc\settings\models\Item;
use ptmc\settings\storage\Manager;
use ptmc\settings\types\Type;
use yii\base\BootstrapInterface;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\base\InvalidParamException;
use yii\helpers\ArrayHelper;

/**
 * Class Settings
 * Класс по работе с настройками приложения. Позволяет создавать, проверять, а так же удалять
 * конфигурационные записи модулей. Получает все записи из хранилища при старте приложения и
 * вносит их в кеш. Обновляет кеш, только после изменения содержимого записей в хранилище.
 *
 * ```php
 * bootstrap => ['log', 'settings', ...],
 * components => [
 *   ...,
 *  'settings' => [
 *    'class' => 'ptmc\settings\Settings',
 * ]
 * ```
 * Может быть так же определены значения по умолчанию, или даполнены типы данных:
 * ```php
 * components => [
 *   ...,
 *  'settings' => [
 *    'class' => 'ptmc\settings\Settings',
 *    'dataTypes' => [
 *       'dictionary' => 'app\components\types\DictionaryType'
 *    ],
 *    'storage' => [
 *       'class' => 'app\components\storage\DbManager',
 *       'tableName' => 'config'
 *    ]
 *   ]
 * ]
 * ```
 * Может использовать сложные типы данных, а работать с ними как с примитивными.
 * ```php
 * Settings::get('app.site.name'); //Вернет экземпляр который реализовал интерфейс Type
 * Settings::get('app.site.enabled').isTrue(); //вернет true если сайт включен, или false если выключен.
 * echo Settings::get('app.site.name'); //Вернет название сайта
 * ```
 * Что бы было регистрировать новые записи в settings, используется специальный тип данных @see Item.
 * Что бы создать его экземпляр и наполнить данными, можно использовать метод
 * ```php
 *  $item = Settings::buildItem([
 *    'key' => 'app.site.enabled',
 *    'value' => true,
 *    'defaultValue' => true,
 *    'label' => 'Позволяет включить или выключить сайт',
 *    'description' => 'Бывает полезно выключить сайт во время технического обслуживания',
 *    'module_id' => 'application',
 *    'value_type' => 'boolean'
 * ]);
 *
 * Settings::set($item); //Если элмент существовал, то поменяет, если нет то будет создан новый
 * ```
 * @package ptmc\settings
 */
class Settings extends Component implements BootstrapInterface
{
    const GLOBAL_MODULE_NAME = 'application';


    public $cachedEnable = !YII_DEBUG;

    public $dataTypes = [];

    public $storage = [
        'class' => 'ptmc\settings\storage\db\Manager'
    ];

    protected $params = [];

    /**
     * @var FactoryType
     */
    protected $factory;


    /**
     * @var Manager
     */
    private $_manager;


    /**
     * @param $key
     * @param string $moduleId
     * @return types\Type
     */
    public static function get($key, $moduleId = Settings::GLOBAL_MODULE_NAME)
    {
        if (!\Yii::$app->settings->hasParam($key, $moduleId)) {
            throw new InvalidParamException(sprintf("Not found param %s for module %s", $key, $moduleId));
        }
        return \Yii::$app->settings->getParam($key, $moduleId);
    }

    /**
     * @param Item $item
     * @return mixed
     */
    public static function set($key, $value, $module_id, $daultValue = null, $label = null, $description = null)
    {
        if(\Yii::$app->settings->hasParam($key, $module_id)){
            return \Yii::$app->settings->setParam($key, $value, $module_id, $daultValue, $label, $description);
        }else{
            return \Yii::$app->settings->addParam($key, $value, $module_id, $daultValue, $label, $description);
        }
    }

    public static function remove(Item $item)
    {
        if(\Yii::$app->settings->hasParam($item->key, $item->moduleId)){
            return \Yii::$app->settings->removeItem($item);
        }
    }

    /**
     * Создает экземпляр item и задает переданные
     * в виде массива свойства.
     * @see Item
     * @param $config
     * @return object | Item
     */
    public static function buildItem($config)
    {
        return \Yii::configure(new Item(), $config);
    }

    public function bootstrap($app)
    {
        if ($this->cachedEnable && \Yii::$app->cache->get('settings')) {
            $this->params = \Yii::$app->cache->get('settings');
        } else {
            $this->update();
        }
    }

    /**
     * Объденеяет типы ядра с пользовательскими и создает на их базе фабрику
     *
     */
    public function init()
    {
        $types = ArrayHelper::merge($this->coreTypes(), $this->dataTypes);
        $this->factory = new FactoryType($types);
    }

    /**
     * @param $key
     * @param $value
     * @param $moduleId
     * @param null $defaultValue
     * @param null $label
     * @param null $description
     * @return bool|Item
     */
    public function addParam($key, $value, $moduleId, $defaultValue = null, $label = null, $description = null)
    {
        $item = Settings::buildItem([
            'key' => $key,
            'param' => $value instanceof Type ? $value : $this->factory->createType('string', $value),
            'defaultParam' => $defaultValue instanceof Type ? $defaultValue : $this->factory->createType('string', $defaultValue),
            'label' => $label,
            'description' => $description,
            'moduleId' => $moduleId,
            'isNewRecord' => true
        ]);
        $result = $this->getManager()->insert($item);
        if($result){
            $this->update();
            return $item;
        }
        return false;
    }

    /**
     * @param $key
     * @param $value
     * @param $moduleId
     * @param null $defaultValue
     * @param null $label
     * @param null $description
     * @return bool|Item
     */
    public function setParam($key, $value, $moduleId, $defaultValue = null, $label = null, $description = null)
    {
        $item = $this->params[$moduleId][$key];
        if($item && $item instanceof Item){
            $item->param = $value instanceof Type ? $value : $this->factory->createType('string', $value);
            if($defaultValue){
                $item->defaultParam = $defaultValue instanceof Type ? $defaultValue : $this->factory->createType('string', $defaultValue);
            }
            if($label){
                $item->label = $label;
            }
            if($description){
                $item->description = $description;
            }
            $result = $this->getManager()->update($item);
            if($result){
                $this->update();
                return $item;
            }
        }
        return false;
    }

    /**
     * @param Item $item
     * @return mixed
     */
    public function removeParam(Item $item)
    {
        $result = $this->getManager()->remove($item);
        if($result){
            $this->update();
        }
        return $result;
    }

    /**
     * @param $key
     * @param string $moduleId
     * @return types\Type
     */
    public function getParam($key, $moduleId = Settings::GLOBAL_MODULE_NAME)
    {
        return $this->params[$moduleId][$key];
    }


    /**
     * Проверяем на существование обязательных параметров moduleId и key
     */
    public function hasParam($key, $moduleId = Settings::GLOBAL_MODULE_NAME)
    {
        return isset($this->params[$moduleId]) && isset($this->params[$moduleId][$key]);
    }

    /**
     * Возвращает коллекцию настроек приложения или конкретного модуля
     * @param null $moduleId
     * @return Item[]
     * @throws NotFoundSettingsModule
     */
    public function getParamsAll($moduleId = null)
    {
        if(!$moduleId){
            return $this->params;
        }

        if(isset($this->params[$moduleId])){
            return $this->params[$moduleId];
        }
        throw new NotFoundSettingsModule();

    }

    public function coreTypes()
    {
        return [
            'string' => 'ptmc\settings\types\StringType',
            'integer', 'ptmc\settings\types\IntegerType',
            'list' => 'ptmc\settings\types\ListType'
        ];
    }

    /**
     * @return object|Manager
     * @throws InvalidConfigException
     */
    private function getManager()
    {
        if(!$this->_manager){
            $this->_manager = \Yii::createObject($this->storage);
        }
        return $this->_manager;
    }

    /**
     * Обновляет состояние settings, получает снова все данные из
     * storage manager и наполняет кеш. Обычно вызывается после изменения в таблице
     * settings (обновление, удаление, добавление).
     */
    private function update()
    {
        $params = $this->getManager()->findAll();
        if($params && !empty($params)){
            foreach ($params as $config) {
                $item = Settings::buildItem([
                    'key' => $config['param_name'],
                    'param' => $this->factory->createType($config['param_type'], $config['param_value']),
                    'defaultParam' => $this->factory->createType($config['param_type'], $config['param_default_value']),
                    'label' => $config['label'],
                    'description' => $config['description'],
                    'moduleId' => $config['module_id'],
                    'valueType' => $config['param_type']
                ]);
                $this->params[$config['module_id']][$config['param_name']] = $item;
            }
        }
        if($this->cachedEnable){
            \Yii::$app->cache->set('settings', $this->params);
        }
    }

}
