<?php
/**
 * @author B.Pavel <kiliman6aro@gmail.com>
 * @copyright 2017
 */

namespace ptmc\settings;


use ptmc\settings\types\Type;

/**
 * Class FactoryType
 * @package ptmc\settings
 */
class FactoryType
{
    protected $types = [];

    /**
     * FactoryType constructor.
     * @param array $types
     */
    public function __construct(array $types)
    {
        $this->types = $types;
    }


    /**
     * Создает экземпляр Type и если переданы данные в виде
     * json задает их созданному экземпляру.
     * @param string $type тип в текстовом виде
     * @param string $data данные в виде строки json
     * @return Type
     * @throws InvalidTypeClass
     * @throws NotFoundTypeClass
     */
    public function createType($type, $data = null)
    {
        if(!isset($this->types[$type])){
            throw new NotFoundTypeClass(sprintf("Not found class for type %s", $type));
        }
        $class = $this->types[$type];
        $type = new $class;
        if(!$type instanceof Type){
            throw new InvalidTypeClass(sprintf("Item must be Type"));
        }
        if(isset($data)){
            $type->set($data);
        }
        return $type;
    }
}


class NotFoundTypeClass extends \Exception {}

class InvalidTypeClass extends \Exception {}