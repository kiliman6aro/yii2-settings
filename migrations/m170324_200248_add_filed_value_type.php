<?php

use yii\db\Migration;

class m170324_200248_add_filed_value_type extends Migration
{
    public function up()
    {
        $this->addColumn('{{%settings}}', 'value_type', "ENUM('boolean', 'integer','string','text', 'list','object','json')");
    }

    public function down()
    {
        $this->dropColumn('{{%settings}}', 'value_type');
        return true;
    }
}
