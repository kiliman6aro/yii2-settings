<?php


class m170321_120311_init_settings extends \yii\db\Migration
{
    public function up()
    {
        $this->createTable('{{%settings}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'label' => $this->string()->notNull(),
            'description' => $this->text(),
            'value' => $this->integer(),
            'default_value' => $this->integer()->defaultValue(1),
            'module_id' => $this->string()->unique(),
        ]);

    }

    public function down()
    {
        $this->dropTable('{{%settings}}');
        return true;
    }

}
