<?php

use yii\db\Migration;

class m170329_094202_change_type_field_enum_to_string extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%settings}}', 'value_type', $this->string()->defaultValue('string'));
        return true;
    }

    public function down()
    {

        $this->alterColumn('{{%settings}}', 'value_type', "ENUM('boolean', 'integer','string','text', 'list','object','json')");
        return true;
    }
}
