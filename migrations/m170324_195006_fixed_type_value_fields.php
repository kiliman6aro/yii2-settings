<?php

use yii\db\Migration;

class m170324_195006_fixed_type_value_fields extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%settings}}', 'value', $this->text()->notNull());
        $this->alterColumn('{{%settings}}', 'default_value', $this->text()->notNull());
    }

    public function down()
    {
        $this->alterColumn('{{%settings}}', 'value', $this->integer());
        $this->alterColumn('{{%settings}}', 'default_value', $this->integer());
        return true;
    }
}
