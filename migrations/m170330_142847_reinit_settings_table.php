<?php

use yii\db\Migration;

class m170330_142847_reinit_settings_table extends Migration
{
    public function up()
    {
        $this->dropTable('{{%settings}}');
        $this->createTable('{{%settings}}', [
            'id' => $this->primaryKey(),
            'param_name' => $this->string()->notNull(),
            'param_value' => $this->string()->notNull(),
            'param_type' => $this->string()->notNull(),
            'module_id' => $this->string()->notNull(),
            'param_default_value' => $this->string(),
            'label' => $this->string(),
            'description' => $this->text()
        ]);
        $this->createIndex('idx_uniq_module_id_and_param_name', '{{%settings}}', ['module_id', 'param_name'], true);
    }

    public function down()
    {
        $this->dropIndex('idx_uniq_module_id_and_param_name', '{{%settings}}');
        $this->dropTable('{{%settings}}');

        $this->createTable('{{%settings}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'value' => $this->string()->notNull(),
            'value_type' => $this->string()->notNull(),
            'module_id' => $this->string()->notNull(),
            'default_value' => $this->string(),
            'label' => $this->string(),
            'description' => $this->text()
        ]);
        $this->createIndex('module_id', '{{%settings}}', ['module_id']);
        return true;
    }
}
