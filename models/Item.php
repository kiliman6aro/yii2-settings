<?php
/**
 * @author B.Pavel <kiliman6aro@gmail.com>
 * @copyright 2017
 */

namespace ptmc\settings\models;


use ptmc\settings\Param;
use ptmc\settings\types\Type;

class Item implements Param
{
    /**
     * @var string ключ значения в таблице конфигов
     */
    public $key;

    /**
     *
     * @var string название используемое для вывода в GUI
     */
    public $label;

    /**
     * @var string описание используемое для вывода в GUI
     */
    public $description;


    /**
     * Может быть строкой, или сложным типом, реализующий интерфейс Type
     * @var Type
     */
    public $param;

    /**
     * Значение по умолчанию, когда нет значения, подставляется это значение, если оно задано
     * Может быть строкой, или сложным типом, реализующий интерфейс Type
     * @var Type
     */
    public $defaultParam;

    /**
     * @var string тип данных
     */
    public $valueType;

    /**
     * @var string название модуля к которому относится данный элемент конфига
     */
    public $moduleId;


    /**
     * @var bool
     */
    public $isNewRecord = true;


    /**
     * Проверяет задано ли основное значение, если задано то возвращает его
     * если нет, то возвращает значение по умолчанию
     * @return Type
     */
    public function getParam()
    {
        return $this->param ? $this->param : $this->defaultParam;
    }


    public function isTrue()
    {
        return $this->getParam()->getValue() == true;
    }

    public function isFalse()
    {
        return $this->getParam()->getValue() == false;
    }

    public function isEmpty()
    {
        return empty($this->getParam()->getValue());
    }

    public function isNotEmpty()
    {
        return !empty($this->getParam()->getValue());
    }

    public function __toString()
    {
        return $this->getParam()->getValue();
    }


}