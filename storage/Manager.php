<?php
/**
 * @author B.Pavel <kiliman6aro@gmail.com>
 * @copyright 2017
 */

namespace ptmc\settings\storage;


use ptmc\settings\models\Item;

interface Manager
{
    public function insert(Item $item);

    public function update(Item $item);

    public function remove(Item $item);

    public function find($name, $module_id);

    public function findAll();

}