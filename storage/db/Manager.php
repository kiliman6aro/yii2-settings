<?php
/**
 * @author B.Pavel <kiliman6aro@gmail.com>
 * @copyright 2017
 */

namespace ptmc\settings\storage\db;


use ptmc\settings\models\Item;

class Manager implements \ptmc\settings\storage\Manager
{
    public $tableName = 'settings';


    /**
     * @param Item $item
     * @return bool
     */
    public function insert(Item $item)
    {
        try{
            \Yii::$app->getDb()->createCommand()->insert($this->tableName, [
                'param_name' => $item->key,
                'param_value' => $item->param ? $item->param->get() : null,
                'param_type' => $item->getParam()->typeName(),
                'module_id' => $item->moduleId,
                'label' => $item->label,
                'description' => $item->description,
                'param_default_value' => $item->defaultParam ? $item->defaultParam->get() : null
            ])->execute();
            return true;
        }catch (\Exception $e){
            \Yii::error(sprintf("Error update setting item with key: %s and module: %s. Reason: %s",
                $item->key, $item->moduleId, $e->getMessage()));
            return false;
        }
    }

    /**
     * @param Item $item
     * @return bool
     */
    public function update(Item $item)
    {
        try{
            \Yii::$app->getDb()->createCommand()->update($this->tableName, [
                'param_value' => $item->param ? $item->param->get() : null,
                'param_type' => $item->getParam()->typeName(),
                'label' => $item->label,
                'description' => $item->description,
                'param_default_value' => $item->defaultParam ? $item->defaultParam->get() : null
            ], 'param_name = :key AND module_id = :module_id', [':key' => $item->key, ':module_id' => $item->moduleId])->execute();
            return true;
        }catch (\Exception $e){
            \Yii::error(sprintf("Error update setting item with key: %s and module: %s. Reason: %s", $item->key,
                $item->moduleId, $e->getMessage()));
            return false;
        }
    }

    /**
     * @param Item $item
     * @return bool
     */
    public function remove(Item $item)
    {
        try{
            \Yii::$app->getDb()->createCommand()->delete($this->tableName,
                'param_name = :key AND module_id = :module_id', [':key' => $item->key, ':module_id' => $item->moduleId])->execute();
            return true;
        }catch (\Exception $e){
            \Yii::error(sprintf("Error remove setting item with key: %s and module: %s. Reason: %s",
                $item->key, $item->moduleId, $e->getMessage()));
            return false;
        }

    }

    /**
     * @param $name
     * @param $module_id
     * @return array|false
     */
    public function find($name, $module_id)
    {
        try{
            $item = \Yii::$app->getDb()->createCommand("SELECT * FROM {$this->tableName} WHERE param_name = :param_name AND module_id = :module_id")
                ->bindValue(':param_name', $name)
                ->bindValue(':module_id', $module_id)->queryOne();
            return $item;
        }catch (\Exception $e){
            \Yii::error(sprintf("Error found settings item: %s in for module: %s. Reason: %s",
                $name, $module_id, $e->getMessage()));
        }
    }

    /**
     * @return array
     */
    public function findAll()
    {
        try{
            $items = \Yii::$app->getDb()->createCommand("SELECT * FROM {$this->tableName}")
                ->queryAll();
            return $items;
        }catch (\Exception $e){
            \Yii::error(sprintf("Error found settings items. Reason: %s", $e->getMessage()));
        }
    }


}