<?php
/**
 * @author B.Pavel <kiliman6aro@gmail.com>
 * @copyright 2017
 */

namespace ptmc\settings\tests\unit;


use ptmc\settings\types\StringType;

class StringTypeTest extends \PHPUnit_Framework_TestCase
{

    public function testCreator()
    {
        $var = new StringType("Hello");
        $this->assertEquals('Hello', $var->getValue());

        $this->assertEquals('Hello', $var->get());
        $var->set("Test");

        $this->assertEquals('Test', $var->getValue());
        $this->assertEquals('Test', $var->get());
    }
}