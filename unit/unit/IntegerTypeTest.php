<?php
/**
 * @author B.Pavel <kiliman6aro@gmail.com>
 * @copyright 2017
 */

namespace ptmc\settings\tests\unit;


use ptmc\settings\types\IntegerType;

class IntegerTypeTest extends \PHPUnit_Framework_TestCase
{

    public function testCreator()
    {
        $integer = new IntegerType(1);
        $this->assertEquals(1, $integer->getValue());
    }
}