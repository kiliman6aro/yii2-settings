<?php
/**
 * @author B.Pavel <kiliman6aro@gmail.com>
 * @copyright 2017
 */

namespace ptmc\settings\tests\unit;


use ptmc\settings\types\ListType;

class ListTypeTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @expectedException \ptmc\settings\InvalidTypeSignatureException
     */
    public function testListTypeExceptions()
    {
        $list = new ListType(['en' => 'English', 'ru' => 'Russian'], 0);
        $this->assertEquals('English', $list->getValue());

        $list = new ListType(['en' => 'English', 'ru' => 'Russian']);
        $this->assertEquals('English', $list->getValue());
    }

    public function testCreate()
    {
        $list = new ListType(['en' => 'English', 'ru' => 'Russian'], 'en');
        $this->assertEquals('English', $list->getValue());
    }
}