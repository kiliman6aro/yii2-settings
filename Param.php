<?php
/**
 * @author B.Pavel <kiliman6aro@gmail.com>
 * @copyright 2017
 */

namespace ptmc\settings;


/**
 * Interface Param
 * Параметр который возвращает Settings, экземпляры реализующие данный интерфейс
 * могут быть поддаваться логическим проверкам.
 *
 * Settings::get('app.site.enabled').isTrue();
 * Settings::get('app.site.name').isNotEmpty();
 * Settings::get('app.site.lang').isNotEmpty();
 * @package ptmc\settings
 */
interface Param
{
    /**
     * @return bool
     */
    public function isTrue();

    /**
     * @return bool
     */
    public function isFalse();

    /**
     * @return bool
     */
    public function isEmpty();

    /**
     * @return bool
     */
    public function isNotEmpty();


    /**
     * Синоним метода getValue(). Обычно реализация возвращает результат
     * вызова getValue();
     * @return string
     */
    public function __toString();

}