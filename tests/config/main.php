<?php

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');

$config = [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'enableCoreCommands' => false,
    'defaultRoute' => false,
    'bootstrap' => ['log', 'settings'],
    'controllerNamespace' => 'app\commands',
    'extensions' => [
        'ptmc/yii2-settings' => [
            'name' => 'ptmc/yii2-settings',
            'version' => '2.0.6.0',
            'alias' => [
                "@ptmc/settings" => dirname(__DIR__).'/extensions/yii2-settings'
            ]
        ]
    ],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'settings' => [
            'class' => 'ptmc\settings\Settings'
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
    ],
    'params' => $params,
    /*
    'controllerMap' => [
        'fixture' => [ // Fixture generation command line.
            'class' => 'yii\faker\FixtureController',
        ],
    ],
    */
];
return $config;
