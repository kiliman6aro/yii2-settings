<?php
/**
 * Тип данных с которым работает модуль Settings. Это позволяет работать
 * со сложными типами данных, или даже пользовательскими типами.
 *
 * Бывает необходимость хранить в базе данных сложные наборы конфигурации, JSON, Object,
 * List, Dictionary и т.д.
 * @author B.Pavel <kiliman6aro@gmail.com>
 * @copyright 2017
 */

namespace ptmc\settings\types;


interface Type {

    /**
     * Задает сложные данные, с которыми будет работать конкретная реализация.
     *
     * @param mixed $data
     * @return bool
     */
    public function set($data);

    /**
     * Возвращает сложные данные, как есть.
     * @return mixed
     */
    public function get();


    /**
     * Возвращает имя типа.
     * @return string
     */
    public function typeName();

    /**
     * Преобразовывает сложный тип данных в строку.
     * Сообщает состояние объекта.
     * @return string
     */
    public function getValue();
}