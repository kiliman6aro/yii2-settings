<?php
/**
 * @author B.Pavel <kiliman6aro@gmail.com>
 * @copyright 2017
 */

namespace ptmc\settings\types;


class StringType extends BaseType
{


    /**
     * StringType constructor.
     */
    public function __construct($data = null)
    {
        if($data){
            $this->set($data);
        }
    }

    public function getValue()
    {
        return $this->get();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->get();
    }

    public function typeName()
    {
        return 'string';
    }


}