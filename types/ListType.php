<?php
/**
 * @author B.Pavel <kiliman6aro@gmail.com>
 * @copyright 2017
 */

namespace ptmc\settings\types;


use ptmc\settings\InvalidTypeSignatureException;

/**
 *  * Список, имеет структуру
 * ```php
 * [
 *  'current' => 'en',
 *  'items' => [
 *     'en' => 'English',
 *     'ru' => 'Russian',
 *     'uk' => 'Ukrainian',
 *  ]
 * ]
 * ```
 *
 * Может быть создан через конструктор
 * ```php
 * $list = new ListType(['en' => 'English', 'ru' => 'Russian', 'uk' => 'Ukrainian'], 'en');
 * echo $list; //English
 * $list->getValue(); //English
 * ```
 * @package ptmc\settings\types
 */
class ListType extends BaseType
{

    /**
     * Если items пустой то проверять правильность структуры нет смысла
     * ListType constructor.
     * @param array|null $items
     * @param null $current
     * @throws InvalidTypeSignatureException
     */
    public function __construct(array $items = null, $current = null)
    {
        if(!empty($items)){
            if(!isset($items[$current])){
                throw new InvalidTypeSignatureException(sprintf("Not found item in items by index: %s", $current));
            }
            $array = [
                'current' => $current,
                'items' => $items
            ];
            $this->data = json_encode($array);
        }
    }

    public function typeName()
    {
        return 'list';
    }


    /**
     * @param mixed $data данные в исходном виде
     * @return bool
     * @throws InvalidTypeSignatureException
     */
    public function set($data)
    {
        $array = json_decode($data, JSON_OBJECT_AS_ARRAY);
        if(!isset($array['current'])){
            throw new InvalidTypeSignatureException("Current key is required for List type");
        }
        if(!isset($array['items'])){
            throw new InvalidTypeSignatureException("Items key is required for List type");
        }
        if(empty($array['items'])){
            throw new InvalidTypeSignatureException("Items must be non empty");
        }
        $this->data = $data;
        return true;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        $array = json_decode($this->get(), JSON_OBJECT_AS_ARRAY);
        $index = $array['current'];
        return $array['items'][$index];
    }
}