<?php
/**
 * @author B.Pavel <kiliman6aro@gmail.com>
 * @copyright 2017
 */

namespace ptmc\settings\types;


abstract class BaseType implements Type
{
    protected $data;

    public function set($data)
    {
        $this->data = $data;
    }

    public function get()
    {
        return $this->data;
    }
}
