<?php
/**
 * @author B.Pavel <kiliman6aro@gmail.com>
 * @copyright 2017
 */

namespace ptmc\settings\types;


class IntegerType extends BaseType
{

    /**
     * IntegerType constructor.
     */
    public function __construct($i = null)
    {
        if(is_int($i)){
            $this->set($i);
        }
    }

    public function typeName()
    {
        return 'integer';
    }

    /**
     * Возваращает значение в виде integer
     * @return int
     */
    public function getValue()
    {
        return (int)$this->get();
    }
}