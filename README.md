Settings
========
Компонент для работы с настройками приложения и модулей. Каждый модуль может 
зарегестрировтаь свои настройки и в дальнейшем работать с ними. Позволяет работать
с различными типами данных в том числе и пользовательскими

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Для загрузки файлов в composer.json добавить


```
"require":{
    ...,
    "ptmc/yii2-settings": "*"
}
"repositories":[{
        "type": "git",
        "url": "http://gitlab.pfsoft.net/ptmc/yii2-settings.git"
}],
```
Выполнить миграцию 
```php
yii migrate --migrationPath="@vendor/ptmc/yii2-settings/migrations"
```


Тестирование
---------------

composer install -dev

```php
vendor\bin\phpunit unit
```

Для использования

```php
"bootstrap" => ['log', 'settings', ...]

"components" => [
    ...,
    'settings' => [
        'class' => 'ptmc\settings\Settings'
    ]
]
```